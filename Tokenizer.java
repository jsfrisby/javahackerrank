// https://www.hackerrank.com/challenges/java-string-tokens/problem

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        
        // Write your code here.
        s.trim();
        StringTokenizer tokenizer = new StringTokenizer(s, ("[_\\@!?.', ]"));
        int token = tokenizer.countTokens();
        System.out.println(token);
        
        while(tokenizer.hasMoreTokens()) {
            System.out.println(tokenizer.nextToken());
        }

        scan.close();
    }
}

