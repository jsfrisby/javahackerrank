// https://www.hackerrank.com/challenges/lowest-triangle/problem

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class LowestTriangle{

    static int lowestTriangle(int base, int area){
        // Complete this function
        // area = 1/2 * base * height
        int smallestHeight = area / base * 2;

		// handle int heights i.e. 2 * 2 / 2 = 2
        if (smallestHeight * base / 2 >= area) {
            return smallestHeight;
        }
        else {
			// handle non int heights i.e. 2 * 100 / 17 = 11.76
            while (smallestHeight * base / 2 < area) {
                smallestHeight++;
            }

            return smallestHeight;
        }

        // alternate solution
        //int minHeight = (int)Math.ceil((2.0*area)/base);
        //return minHeight;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int base = in.nextInt();
        int area = in.nextInt();
        int height = lowestTriangle(base, area);
        System.out.println(height);
    }
}
