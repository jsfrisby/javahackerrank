// https://www.hackerrank.com/challenges/java-loops-i/problem

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class LoopsI {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();

		// possible solution
        //for (int i = 1; i < 11; i++) {
        //    System.out.println(N + " x " + i + " = " + N * i);
        //}

		// alternate solution
        if (N >= 2 && N <= 20)
		{
			int result;

			for (int i = 1; i < 11; i++)
			{
				result = N * i;
				System.out.println(N + " x " + i + " = " + result);
			}
        }
    }
}
