// https://www.hackerrank.com/challenges/java-biginteger/problem
// https://www.tutorialspoint.com/java/math/java_math_biginteger.htm
// https://www.tutorialspoint.com/java/math/biginteger_add.htm
// https://www.tutorialspoint.com/java/math/biginteger_multiply.htm

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
//import java.math.BigInteger; // need this line
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        // https://docs.oracle.com/javase/7/docs/api/java/math/BigInteger.html
        Scanner scan = new Scanner(System.in);
        BigInteger bigInt1 = new BigInteger(scan.next());
        BigInteger bigInt2 = new BigInteger(scan.next());

        BigInteger sum;
        BigInteger product;
        sum = bigInt1.add(bigInt2);
        product = bigInt1.multiply(bigInt2);
        System.out.println(sum);
        System.out.println(product);
    }
}
