// https://www.hackerrank.com/challenges/java-exception-handling-try-catch/problem
// helpful example https://beginnersbook.com/2013/04/try-catch-in-java/

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scan = new Scanner(System.in);

        try{
            int x = scan.nextInt();
            int y = scan.nextInt();

            System.out.println(x / y);
        } catch(InputMismatchException ie) {
            System.out.println("java.util.InputMismatchException");
        } catch (ArithmeticException e) {
            System.out.println("java.lang.ArithmeticException: / by zero");
        } catch(Exception e) {
            System.out.println(e.toString());
        }
    }
}
