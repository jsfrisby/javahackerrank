import java.util.*;
import java.io.*;

class JavaLoopsII{
    public static void main(String []argh){
        Scanner in = new Scanner(System.in);
        int t=in.nextInt();
        
        String buildStr = "";
        int result = 0;
        
        for(int i=0;i<t;i++) {
            int a = in.nextInt();
            int b = in.nextInt();
            int n = in.nextInt();
            
            for (int j = 1; j <= n; j++) {
                result += a;
                for (int k = 1; k <= j; k++) {
                    result += Math.pow(2,k-1) * b;
                }
                
                buildStr += result + " ";
                
                
                result = 0;
            }
            System.out.println(buildStr);
            buildStr = "";
        }
        
        in.close();
    }
}
