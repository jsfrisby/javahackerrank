import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the staircase function below.
    static void staircase(int n) {
        // first for loop handles the lines that are printed
        for (int i = 0; i < n; i++)
        {
            // handles the spaces that are printed
            for (int j = n - i; j > 1; j--)
            {
                // print not println
                // because we don't want to move
                // to the next line yet
                System.out.print(" ");
            }
            
            // handles the # that are printed
            for (int k = 0; k < i + 1; k++)
            {
                System.out.print("#");
            }
            
            // finished printing; move to the next line
            System.out.println();
        }

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        staircase(n);

        scanner.close();
    }
}
